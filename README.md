USE:

Access Finding Items Advanced API 

    request: 
    <keywords> {KEYWORD}  <keywords>
    <sortOrder> BestMatch </sortOrder>

To Run:

sudo docker run --rm --privileged -it --name svc-spectre-bg-rank-downloader -v $PWD:/go/src/svc-spectre-bg-rank-downloader -v $HOME/.ssh:/root/.ssh -v /data/sourcingtool:/data/sourcingtool  --env-file .envSandbox -w /go/src/svc-spectre-bg-rank-downloader billyteves/alpine-golang-glide:1.2.0 bash